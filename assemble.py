import os
import sys
import yaml
import time
import thread
import glob
import zipfile
import itertools
import shutil
import string
import subprocess

from spades import spades
from multiprocessing import Pool

# Import settings listed out in settings.yml file
def import_settings(yaml_file):
    global settings
    settings = {}
    with open(yaml_file, 'r') as stream:
        try:
            settings = yaml.load(stream)["settings"]
        except yaml.YAMLError as exc:
            print(exc)
    return settings

# Define a file path and make recursive directories
def make_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

# Check to see if a number is even
def is_even(num):
  return (int(num) & 1) == 0

def split_seq(iterable, size):
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))

# A function to download sequences using the sra toolkit.
# This function accepts a string parameter of the sequence name.
# The function will be downloaded to the 'sequences directory' listed in the settings.yml file
def download_sequence(sequence_name):
    savedir = settings["directories"]["sequences"]
    # Construct the path for which the sequence will be stored when downloaded
    sequence_dir = savedir + "/" + sequence_name
    make_directory(sequence_dir)
    # Construct all parameters in the form of a long string
    command = "./sratoolkit/bin/fastq-dump.2.8.1-3"
    command = command + " --outdir " + sequence_dir
    command = command + " --split-files " + sequence_name
    print(os.system(command))

# Read in all of the sequences from a text file
def read_sequences(filename):
    lines = []
    with open(filename) as f:
        try:
            for line in f.readlines():
                lines.append(line.rstrip('\n'))
        except:
            print("Unable to read file!")
    return lines

# Write a list of sequences to a text file
def write_sequences(filename, arraylist):
    with open(filename, 'w') as f:
        try:
            for item in arraylist:
                f.write(item + "\n")
        except:
            print("Unable to write to file!")

def sequence_archive():
    sequences = os.listdir(settings["directories"]["sequence_archive"])
    file_map = dict()
    sequence_names = []
    for sequence in sequences:
        name = sequence.split("_")[0]
        if name not in file_map:
            file_map[name] = [sequence]
        else:
            file_map[name].append(sequence)
    for name in file_map:
        sequences = file_map[name]
        if(is_even(len(sequences))):
            sequence_names.append(name)
    return sequence_names

# A completed genome is an assembled genome with a 'contigs.fasta'
# file that exists
def completed_genomes():
    genome_dir = settings["directories"]["genome_directory"]
    genomes = os.listdir(genome_dir)
    completed_genomes = []
    for genome in genomes:
        genome_name = genome.split("_")[0]
        assembly = genome_dir + "/" + genome_name + "_paired/contigs.fasta"
        cloud_path = genome_dir + "/" + genome_name
        if os.path.exists(assembly):
            completed_genomes.append(genome_name)
        else:
            cloud_assembly = genome_dir + "/" + genome_name# + "/contigs.fasta"
            if os.path.exists(cloud_assembly):
                print("Has found cloud assembly: " + cloud_assembly)
                if(genome_name not in completed_genomes):
                    completed_genomes.append(genome_name)
            #print("Error: " + assembly)
    return completed_genomes

def make_sequence_list(filename):
    completed = completed_genomes()
    all_sequence_names = sequence_archive()
    uncompleted = set(all_sequence_names) - set(completed)
    print(len(uncompleted))
    print(uncompleted)
    write_sequences(filename, uncompleted)

# This method was for the mapped sequences. Something I have not been able to do yet...
# It is best to look at similar logic for the assembled_unmapped_sequences method
def assemble_mapped_sequences():
    pass

def assemble_unmapped_sequences(sequence_name):
    # Get the directory for which the sequences should be saved
    savedir = settings["directories"]["sequences"]
    # Construct the relative path of which the sequence should be saved
    sequence_dir = savedir + "/" + sequence_name
    # Sort all of the sequences
    sequences = sorted(os.listdir(sequence_dir))
    counter = 1
    command = "pe" # Specific command to spades for paired sequences
    # Only run on even sequences as they each will have a pair.
    # Odd sequences will be skipped since they have no pairs
    if(is_even(len(sequences))):
        # Start constructing the argument
        spades_run_args = ["spades.py"]
        # For each pair...
        for i in range(0,len(sequences),2):
            forward_filename = sequence_dir + "/" + sequences[i]
            reverse_filename = sequence_dir + "/" + sequences[i + 1]
            spades_run_args.append("--" + command + str(counter) + "-1")
            spades_run_args.append(forward_filename)
            spades_run_args.append("--" + command + str(counter) + "-2")
            spades_run_args.append(reverse_filename)
        spades_run_args.append("--careful")
        # Where should spades store the output of the assembled genome
        spades_run_args.extend(["-o", settings["directories"]["mapped_sequences"] + "/" + sequence_name])
        # Try to run the spades command
        try:
            if(len(spades_run_args) > 0):
                spades.main(spades_run_args)
            print(spades_run_args)
        except:
            print("error")

def assemble_genome(sequence):
    download_sequence(sequence)
    assemble_unmapped_sequences(sequence)

# Generate multiple processes of the 'assemble_genome' function
# To view these processes, run the following command in a terminal
# > ps aux | grep "python"
def run(mode, filename):
    proc = Pool(settings["pool"])
    sequences = read_sequences(filename)
    proc.map(assemble_genome, sequences)

# THE MAIN PROGRAM
if __name__ == '__main__':
    command = sys.argv[1]

    if(command == "help"):
        print("Welcome to the genome assembler:")
        print(" help : Access the help menu")
        print(" standup : Setup the workspace")
        print(" sequencelist [settings_file] : Build a list of sequence names given a folder of sequences")
        print(" process mapped [settings_file] : Generated mapped assemblies")
        print(" process unmapped [settings_file] : Generate unmapped assemblies")

    elif(command == "standup"):
        import_settings(sys.argv[2])
        make_directory(settings["directories"]["assemblies_mapped"])
        make_directory(settings["directories"]["assemblies_unmapped"])
        make_directory(settings["directories"]["sequences"])
        make_directory(settings["directories"]["mapped_sequences"])
        make_directory(settings["directories"]["temporary"])

    elif(command == "sequencelist"):
        import_settings(sys.argv[2])
        make_sequence_list(settings["filenames"]["save_file"])

    elif(command == "process"):
        import_settings(sys.argv[3])
        mode = sys.argv[2]
        run(mode, settings["filenames"]["read_file"])

    else:
        print("Parameters are not recognized. Please refer to the help menu.")
        print(">> python assemble.py help")
