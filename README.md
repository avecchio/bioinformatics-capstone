# parallel-assembler
A python wrapper that assembles genomes on a large scale.

Authors:
- Veronica Moorman <vmoorman@kettering.edu>
- Austin Vecchio <au.vecchio@gmail.com>

#### Running
The documentation of this software is intended for the linux machines on Cyverse Atmosphere.
If you are running this software on a non linux machine, please follow the OS Instructions listed below.
1. Add the list of accessions to the 'accessions.txt' file.
2. Modify the settings.yml if needed. The only modification that should ever be changed is the pool parameter.
3. To run the assembly program, use one of the following two commands
- `python assemble.py process unmapped settings.yml` To build unmapped assemblies using spades
- `python assemble.py process mapped settings.yml` To build mapped assemblies using bowtie.
4. To check if the program has finished, run the following linux command on the same machine the program is running on:
ps aux | grep "python"
If there are results that include the command of 'python assemble.py...', then there are still processes running.
If there are no results that include 'python assemble.py...', then you are able to package the assembly files.
5. To download the files if you are using a remote machine:
- navigate to the maps directory: `cd /path/to/maps`
- package all of the assembled programs: `$ for file in *; do zip ${file%.*}.zip $file; done`
  NOTE: This will take a while (hours..)
- move all of the files to the home directory: `mv *.zip ~`
- navigate to the home directory: `cd ~`
- exit out of the remote machine: `exit`
- and finally download all of the zip files: `scp <user>@<ip_address>:*.zip .`
  NOTE: This will take even longer (half to a full day)


#### Operating System Instructions

The 'bin' and 'spades' folders are the linux binaries of the spades toolkit.
The 'sratoolkit' folder is the linux binaries for the SRA Toolkit.
To use this software on a different operating system, download the software from the websites listed below,
extract the programs and add the proper folders to this directory. Delete the old folders first.

Spades: http://cab.spbu.ru/files/release3.10.1/manual.html
SRA Toolkit: https://www.ncbi.nlm.nih.gov/sra/docs/toolkitsoft/
